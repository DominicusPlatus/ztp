﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GpxAnalyser_Data;

namespace GpxAnalyser_Xml
{
    public interface IXmlGpxExtensionReader
    {
        GpxTrack GetTrack();
        GpxTrack ReadTrackFromPath(Uri path);
    }
}
