﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using GpxAnalyser_Data;

namespace GpxAnalyser_Xml
{

    public class XmlGpxTopographicsExtensionReader : IXmlGpxExtensionReader
    {
        private Uri _sourcePath;
        private GpxTrack _sourceGpxTrack;

        public XmlGpxTopographicsExtensionReader()
        {

        }

        public XmlGpxTopographicsExtensionReader(Uri sourcePath)
        {
            this._sourcePath = sourcePath;
        }

        private void LoadTrack()
        {

        }

        public GpxTrack GetTrack()
        {
            return _sourceGpxTrack;
        }

        private void ReadTrackFromGpx()
        {
            var settings = new XmlReaderSettings
            {
                IgnoreComments = true,
                IgnoreProcessingInstructions = true,
                IgnoreWhitespace = true
            };
            using (var reader = XmlReader.Create(_sourcePath.AbsolutePath, settings))
            {
                if (reader.IsStartElement("gpx"))
                {
                    string defaultNamespace = reader["xmlns"];
                    XmlSerializer serializer = new XmlSerializer(typeof(GpxDocument), defaultNamespace);
                    GpxDocument doc = (GpxDocument)serializer.Deserialize(reader);
                    _sourceGpxTrack = doc?.Track;
                }
            }
        }

        private void ReadTrackFromFile()
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(GpxDocument),new XmlRootAttribute("gpx"));
            TextReader textReader = new StreamReader(_sourcePath.AbsolutePath);
            GpxDocument doc  = (GpxDocument)deserializer.Deserialize(textReader);
            _sourceGpxTrack = doc?.Track;
            textReader.Close();
        }

        public GpxTrack ReadTrackFromPath(Uri path)
        {
            this._sourcePath = path;
            ReadTrackFromGpx();
            return _sourceGpxTrack;
        }




    }

}
