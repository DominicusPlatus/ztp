﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GpxAnalyser_Data;
using GpxAnalyser_Xml;
using NUnit.Framework;

namespace GpxAnalyser_Tests
{
    [TestFixture]
    public class GpxReaderTests
    {
        [Test]
        public void Test_GpxReader_ReadsGpx()
        {
            Uri uri = new Uri("G://INF//ZTP//GpxAnalyser_Data//twister.gpx");//new Uri("http://endurotrails.pl/wp-content/uploads/2015/09/twister.gpx");
            IXmlGpxExtensionReader reader = new XmlGpxTopographicsExtensionReader();
            GpxTrack result = reader.ReadTrackFromPath(uri);
            Assert.IsTrue(result != null);
        }

        [Test]
        public void Test_GpxReader_ReadsNodeStructure()
        {
            Uri uri = new Uri("G://INF//ZTP//GpxAnalyser_Data//twister.gpx");//new Uri("http://endurotrails.pl/wp-content/uploads/2015/09/twister.gpx");
            IXmlGpxExtensionReader reader = new XmlGpxTopographicsExtensionReader();
            GpxTrack result = reader.ReadTrackFromPath(uri);
            bool? reads = result.Segments?.FirstOrDefault()?.Points.Any();
            Assert.IsTrue(reads);
        }
        

    }

}
