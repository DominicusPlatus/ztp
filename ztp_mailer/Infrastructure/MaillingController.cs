﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using ztp_mailer.Service.Mailer;
using ztp_mailer.Service.Repository;

namespace ztp_mailer.Infrastructure
{
    public interface IMailingController
    {
        
    }

    public class MaillingController
    {
        public IMailingService Service { get; set; }
        public IRecipientDataRepository Repository { get; set; }
        public ILogger Logger { get; set; }

        public MaillingController(IMailingService service, IRecipientDataRepository repository, ILogger logger)
        {
            Service = service;
            Repository = repository;
            Logger = logger;
        }

    }
}
