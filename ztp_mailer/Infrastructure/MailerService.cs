﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using Autofac;
using FluentMailer.Factory;
using FluentMailer.Interfaces;
using Serilog;
using Topshelf;
using ztp_mailer.Data.Mailing;
using ztp_mailer.IPC;
using ztp_mailer.Service.Repository;
using ztp_mailer.Service.Repository.CSV;
using Topshelf;
using ztp_mailer.Infrastructure;

namespace ztp_mailer.Service.Mailer
{
    public class MailerRuntime : IStartable // : ServiceControl 
    {
        public Timer TickTimer { get; set; }
        public IMailingIpcMessanger Messanger { get; set; }
        public IMailingService Service { get; set; }
        public IRecipientDataRepository Repository { get; set; }
        public ILogger Logger { get; set; }

        public MailerRuntime(IMailingService service, IRecipientDataRepository repository, ILogger logger)
        {
            Service = service;
            Logger = logger;
            Repository = repository;
            Mailer_StartTimer();
        }

        public void  Mailer_StartTimer()
        {
            TickTimer = new Timer(1000);
            TickTimer.Elapsed += new ElapsedEventHandler(Mailer_Tick);
            TickTimer.Enabled = true;
        }


        private void Mailer_Tick(object sender, ElapsedEventArgs e)
        {
            // this.Messanger.Raise();
            Service.SendMessageToRecipientsAsync(new MaillingMessage(), Repository.LoadRecipientsFromSource(100));
        }

        void Mailer_Stop_Services()
        {
            TickTimer.Stop();
        }

        void Mailer_Bootstrap_Services()
        {
         //   var builder = new ContainerBuilder();

        }


        public void Start()
        {
            Mailer_Bootstrap_Services();
            Mailer_StartTimer();
        }

        public void Stop()
        {
            Mailer_Stop_Services();
        }
    }

}
