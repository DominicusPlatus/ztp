﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ztp_mailer.Data.Mailing;

namespace ztp_mailer.Service.Repository
{
    public interface IRecipientDataRepository
    {
        void SetDataSource(string path);
        List<MailingRecipient> LoadRecipientsFromSource(int number);


    }
}
