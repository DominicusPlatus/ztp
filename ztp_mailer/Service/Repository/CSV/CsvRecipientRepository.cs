﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Serilog;
using Serilog.Events;
using ztp_mailer.Data.Mailing;

namespace ztp_mailer.Service.Repository.CSV
{
    public class CsvRecipientRepository : IRecipientDataRepository
    {

        public string  Path { get; set; }
        public ICsvReader Reader { get; set; }
        public ILogger Logger { get; set; }

        private List<MailingRecipient> Recipients; 


        public CsvRecipientRepository(ILogger logger, ICsvReader reader)
        {
            Logger = logger;
            Reader = reader;
            Recipients = new List<MailingRecipient>();
        }

        private bool IsDataSourceCorrect()
        {
            return false;
        }

        public void SetDataSource(string path)
        {
            Path = path;
        }


        public List<MailingRecipient> LoadRecipientsFromSource(int number)
        {
            try
            {
                if (IsDataSourceCorrect())
                {
                    Recipients = new List<MailingRecipient>();
                    for (int i = 0; i < number; i++)
                    {
                        Reader.Read();
                        MailingRecipient rec = Reader.GetRecord<MailingRecipient>();
                        if (rec != null)
                        {
                            Recipients.Add(rec);
                        }
                    }
                    return Recipients;
                }
                else
                {
                    return new List<MailingRecipient>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex,ex.Message);
                return new List< MailingRecipient > ();
            }
        }
    }
}
