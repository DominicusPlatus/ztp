﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using FluentMailer.Interfaces;
using Serilog;
using Serilog.Events;
using ztp_mailer.Data.Mailing;
using ztp_mailer.IPC;
using ztp_mailer.Service.Repository;

namespace ztp_mailer.Service.Mailer
{
    public class BackgroundMailingService : IMailingService
    {
        public ILogger Logger { get; set; }
        public IFluentMailer Mailer { get; set; }
        public IFluentMailerMailSender Sender { get; set; }
        public bool SendPending   { get; set; }
     //   public IMailingIpcMessanger IpcMessanger { get; set; }
        public IRecipientDataRepository Repository { get; set; }

        public BackgroundMailingService(ILogger logger, IFluentMailer mailer)
        {
            Logger = logger;
            Mailer = mailer;
          ///  IpcMessanger = messanger;
          //  IpcMessanger.MailingTimerElapsed += IpcMessanger_MailingTimerElapsed;


        }

        //private void IpcMessanger_MailingTimerElapsed(object sender, EventArgs e)
        //{
        //    SendMessageToRecipientsAsync(new MaillingMessage(), Repository.LoadRecipientsFromSource(100));
        //}

        public Task SendMessageToRecipientsAsync(MaillingMessage msg, List<MailingRecipient> rcpts)
        {
            try
            {
                SendPending = true;
                var message = Mailer.CreateMessage();
                
                var mailSender = message.WithView("~/Views/Mailer/Mail.cshtml", msg);

                string[] rxList = rcpts.Select(v => v.EMail).ToArray();  
                mailSender.WithReceivers(rxList);

                mailSender.WithSubject("Mail subject");

                //await _fluentMailer.CreateMessage()
                //.WithViewBody("<html><body>Test message</body></html>")
                //.WithReceivers("abc@abc.com", "bcd@bcd.com")
                //.WithSubject("Mail subject")
                //.WithAttachment("~/book.pdf")
                //.SendAsync();

                return mailSender.SendAsync();

            }
            catch (Exception ex)
            {
                Logger.Error(ex,ex.Message);
                return null;
            }
        }

    
    }
}
