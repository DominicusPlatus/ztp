﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ztp_mailer.Data.Mailing;

namespace ztp_mailer.Service.Mailer
{
    public interface IMailingService
    {

        Task SendMessageToRecipientsAsync(MaillingMessage msg, List<MailingRecipient> rcpts);
        bool SendPending { get; set; }
    }
}
