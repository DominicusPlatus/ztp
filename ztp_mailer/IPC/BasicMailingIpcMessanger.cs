﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ztp_mailer.IPC
{
    public class BasicMailingIpcMessanger : IMailingIpcMessanger
    {
        public event EventHandler MailingTimerElapsed;

        protected virtual void OnMailerTick(EventArgs e)
        {
            MailingTimerElapsed?.Invoke(this, e);
        }
        
        public void Raise()
        {
            OnMailerTick(new EventArgs());
        }
    }
}
