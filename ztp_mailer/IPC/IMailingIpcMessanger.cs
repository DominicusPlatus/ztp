﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ztp_mailer.IPC
{
    public interface IMailingIpcMessanger
    {
        event EventHandler MailingTimerElapsed;
        void Raise();
    }
}
