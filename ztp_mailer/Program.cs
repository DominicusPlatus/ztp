﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using CsvHelper;
using FluentMailer.Factory;
using FluentMailer.Interfaces;
using Serilog;
using Topshelf;
using ztp_mailer.IPC;
using ztp_mailer.Service.Mailer;
using ztp_mailer.Service.Repository.CSV;

namespace ztp_mailer
{
    class Program
    {
        static void Main(string[] args)
        {

            var builder = new ContainerBuilder();

            var dataAccess = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(dataAccess).AsImplementedInterfaces();

            builder.RegisterType<BasicMailingIpcMessanger>();
            builder.RegisterType<BackgroundMailingService>();
            builder.RegisterType<CsvRecipientRepository>();

            var fluentMailer = FluentMailerFactory.Create();
            builder.RegisterInstance<IFluentMailer>(fluentMailer);


            builder.Register<ILogger>((c, p) =>
            {
                return new LoggerConfiguration()
                  .CreateLogger();
            }).SingleInstance();

            builder
               .RegisterType<MailerRuntime>()
               .AutoActivate();

            builder.Build();

            //MailerRuntime runtime = new MailerRuntime();
            //runtime.Start();


            //HostFactory.Run(x =>                              
            //{
            //    x.Service<MailerRuntime>(s =>
            //    {
            //        s.ConstructUsing(name => new MailerRuntime());    
            //        s.WhenStarted(tc => tc.Start());             
            //        s.WhenStopped(tc => tc.Stop());              
            //    });
            //  //  x.RunAsLocalSystem();                          
            //    x.RunAsPrompt();
            //});



        }

    }
}
