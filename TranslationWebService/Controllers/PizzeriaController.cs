﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using mongotests;
using Newtonsoft.Json;
using TranslationInfrastracture;
using TranslationService;

namespace TranslationWebService.Controllers
{
    [Authorize]
    public class PizzeriaController : ApiController
    {
        private IPizzaTranslationService TranslationService;

        public PizzeriaController(IPizzaTranslationService service)
        {
            TranslationService = service;
        }

        public string GetRecipeById(string id)
        {
            return JsonConvert.SerializeObject(TranslationService.GetRecipeById(id));
        }

        public string GetAllRecipes()
        {
            return JsonConvert.SerializeObject(TranslationService.GetAllRecipes());
        }


    }
}
