﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GpxAnalyser_Data
{
    //[XmlType("Metadata")]
    //[Serializable]
    //[XmlRoot(ElementName = "gpx")]

    [XmlRoot("gpx")]
    public class GpxDocument
    {
        [XmlElement(ElementName = "trk")]
        public GpxTrack Track { get; set; }
    }

}
