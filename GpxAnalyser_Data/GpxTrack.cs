﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GpxAnalyser_Data
{

    [XmlRoot(ElementName = "trk")]
    [Serializable]
    public class GpxTrack
    {

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }


        [XmlElement(ElementName = "trkseg")]
        public List<GpxTrackSegment> Segments { get; set; }

    }
}
