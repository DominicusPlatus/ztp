﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GpxAnalyser_Data
{

    [XmlRoot(ElementName = "trkpt")]
    [Serializable]
    public class GpxTrackPoint
    {
        [XmlAttribute(AttributeName = "lat")]
        public double Lat { get; set; }

        [XmlAttribute(AttributeName = "lon")]
        public double Lng { get; set; }

        [XmlElement(ElementName = "ele")]
        public double Elevation { get; set; }

        [XmlElement(ElementName = "time")]
        public DateTime Time { get; set; }

    }
}
