﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GpxAnalyser_Data
{
    [XmlRoot(ElementName = "trkseg")]
    [Serializable]
    public class GpxTrackSegment
    {

        //[XmlElement(ElementName = "name")]
        //public string Name { get; set; }

        [XmlElement(ElementName = "trkpt")]
        public List<GpxTrackPoint> Points { get; set; }
    }
}
