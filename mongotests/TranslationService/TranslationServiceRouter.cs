﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using TranslationInfrastracture;
using TranslationService;

namespace TranslationService
{

    public abstract  class RecipeTranslationService : IPizzaTranslationService
    {
        protected RecipeTranslationService successor;
        
        public void SetSuccessor(RecipeTranslationService successor)
        {
            this.successor = successor;
        }

        public abstract Pizza GetRecipeById(string id);
        public abstract List<Pizza> GetAllRecipes();
     
    }

}
