﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using TranslationRepository;


namespace TranslationService
{
    public class ChineseTranslationService : RecipeTranslationService
    {
        private CultureInfo Culture;

        private ITranslationRepository Repository;

        public ChineseTranslationService(ITranslationRepository repository)
        {
            Culture = new CultureInfo("zh-CN");
            Repository = repository;
        }


        public override Pizza GetRecipeById(string id)
        {
            if (Repository.DoesTranslationExistForRecipeLanguage(id, Culture.Name))
            {
                return Repository.GetRecipeByIdInLanguage(id, Culture.Name);
            }
            else
            {
                return successor.GetRecipeById(id);
            }
        }

        public override List<Pizza> GetAllRecipes()
        {
            return Repository.GetRecipesInLanguage(Culture.Name);
        }

    }
}
