﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using TranslationInfrastracture;
using TranslationRepository;
using TranslationService;

namespace mongotests
{
    class Program
    {
        static void Main(string[] args)
        {
            ITranslationRepository repo =  new MongoTranslationRepository();
            RecipeTranslationService serviceEng = new EnglishTranslationService(repo);
            RecipeTranslationService servicePL = new PolishTranslationService(repo);
            RecipeTranslationService serviceCH = new ChineseTranslationService(repo);
            serviceEng.SetSuccessor(servicePL);
            servicePL.SetSuccessor(serviceCH);
        


        }
    }
}
