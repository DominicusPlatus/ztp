﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace mongotests
{

    [BsonIgnoreExtraElements]
    public  class Pizza
    {
        //[BsonId]
        //[BsonElement("_id")]
        //public ObjectId Id {
        //    get
        //    {
        //        return 
        //    } set; }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.Double)]
        public double Price { get; set; }

        public string ImageUrl { get; set; }

        public ICollection<PizzaContent> Contents { get; set; }

    }

    public class PizzaContent
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
    }

}
