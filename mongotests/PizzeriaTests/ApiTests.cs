﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using Newtonsoft.Json;
using NUnit.Framework;
using TranslationInfrastracture;
using TranslationRepository;
using TranslationService;
using TranslationWebService.Controllers;

namespace PizzeriaTests
{

    [TestFixture]
    public class ApiTests
    {
        private IPizzaTranslationService testService;

        [OneTimeSetUp]
        public void Test_Api_Initialize()
        {
            ITranslationRepository Repository = new InMemoryTranslationRepository();
            Repository.Fake();

            //create sample chain
            var serviceEng = new EnglishTranslationService(Repository);
            var servicePL = new PolishTranslationService(Repository);
            var serviceCH = new ChineseTranslationService(Repository);
            serviceEng.SetSuccessor(servicePL);
            servicePL.SetSuccessor(serviceCH);

            testService = serviceEng;
        }

        [Test]
        void Test_Api_Get_Recipe_ById()
        {

        }


        [Test]
        public void Test_Api_GetAll()
        {
            try
            {

                var controller = new PizzeriaController(testService);
                var serialized = controller.GetAllRecipes();
                var rxEvents = JsonConvert.DeserializeObject<List<Pizza>>(serialized);
                Assert.IsTrue(rxEvents.Count > 0);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                Assert.Fail();
            }
        }


    }

}
