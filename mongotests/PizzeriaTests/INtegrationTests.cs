﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using TranslationRepository;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace PizzeriaTests
{

    [TestFixture]
    public class IntegrationTests
    {

        readonly MongoClient _client = new MongoClient("mongodb://localhost:27017");
        private IMongoDatabase database;

        private ITranslationRepository Repository;

        public string TestCollectionName { get; set; }

        [OneTimeSetUp]
        public void Test_Integration_Mongo_Repository_Initialize()
        {
            TestCollectionName = Guid.NewGuid().ToString();
            database = _client.GetDatabase("local");
            Repository = new MongoTranslationRepository();
        }

        [Test]
        public void Test_Integration_Mongo_Repository_HasCollections()
        {
            var collections = new List<BsonDocument>();
            foreach (var item in database.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result)
            {
                collections.Add(item);
            }
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(collections.ToList().Count > 0);
        }


        [Test]
        public void Test_Integration_Mongo_Repository_Recipe_GetById()
        {
            var collection = database.GetCollection<Pizza>("pizza");
            var items = collection.AsQueryable();
            var entity = items.First(i => i.Id.Equals("57e697257ae46124486316c9"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(entity != null && entity.Id.ToString().Equals("57e697257ae46124486316c9"));
        }

        [Test]
        public void Test_Integration_Mongo_Repository_Recipe_GetAll()
        {
            var collection = database.GetCollection<Pizza>("pizza");
            var items = collection.AsQueryable<Pizza>();
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(items.ToList().Any());
        }

        [Test]
        public void Test_Integration_Mongo_Repository_Recipe_GetAll_InLanguage()
        {
            var enrecp = Repository.GetRecipesInLanguage(new CultureInfo("en-GB").Name);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(enrecp?.First().Contents?.Count > 0);   //  && enrecp?.Where(r=>r.Contents.First(i=>!i.LanguageCode.Equals("en-GB"))).ToList().Count() <= 0 
        }

        [Test]
        public void Test_Integration_Mongo_Repository_Recipe_GetById_InLanguage()
        {
            string testId = "57e697257ae46124486316c9";
            var enrecp = Repository.GetRecipeByIdInLanguage(testId, new CultureInfo("en-GB").Name);
            Assert.IsTrue(enrecp?.Contents?.Count == 1 && enrecp.Contents.First().LanguageCode.Equals("en-GB"));  //.Contents.Where(i => !i.LanguageCode.Equals("en-GB")) == null)
        }



    }
}
