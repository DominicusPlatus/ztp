﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using NUnit.Framework;
using TranslationRepository;
using TranslationService;

namespace PizzeriaTests
{
    [TestFixture]
    public class ChainTests
    {
        private RecipeTranslationService serviceEng;
        private RecipeTranslationService servicePL;
        private RecipeTranslationService serviceCH;

        [OneTimeSetUp]
        public void Test_Repository_Initialize()
        {

        }


        [Test]
        public void Test_Chain_Resolve_Request_Recipe_For_Language()
        {
            RecipeTranslationService service;
        }

        [Test]
        public void Test_Chain_Successor_Delegation()
        {
            ITranslationRepository Repository = new InMemoryTranslationRepository();
            Repository.Fake();

            //create sample chain
            serviceEng = new EnglishTranslationService(Repository);
            servicePL = new PolishTranslationService(Repository);
            serviceCH = new ChineseTranslationService(Repository);
            serviceEng.SetSuccessor(servicePL);
            servicePL.SetSuccessor(serviceCH);

            //test chain

            //try to get recipe in one language from service of another language
            var chRecipe = serviceEng.GetRecipeById("57e697257ae46124486316cb");
            Assert.IsTrue(chRecipe.Contents.First().LanguageCode.Equals("zh-CN"));  //recipe existist only for chineese
           
        }

        //[Test]
        //void Test_Chain_Query_NonSupportedLanguage()
        //{
        //    RecipeTranslationService service;
        //}

    }
    
}
