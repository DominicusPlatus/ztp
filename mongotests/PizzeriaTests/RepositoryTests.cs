﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using mongotests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using TranslationRepository;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace PizzeriaTests
{
    [TestFixture]
    public class RepositoryTests
    {
        private ITranslationRepository Repository;

        public string TestCollectionName { get; set; }

        [OneTimeSetUp]
        public void Test_Repository_Initialize()
        {
            TestCollectionName = Guid.NewGuid().ToString();
            Repository = new InMemoryTranslationRepository();
        }

        [Test]
        public void Test_Repository_HasCollections()
        {
            Repository = new InMemoryTranslationRepository();
            Repository.Fake();
            Assert.IsTrue(Repository.GetRecipesInLanguage("zh-CN").ToList().Count > 0);
        }


        [Test]
        public void Test_Repository_Recipe_GetById()
        {
            Repository = new InMemoryTranslationRepository();
            Repository.Fake();
            var items = Repository.GetRecipesInLanguage("zh-CN").AsQueryable();
            var entity = items.First(i => i.Id.Equals("57e697257ae46124486316cb"));
            Assert.IsTrue(entity != null && entity.Id.ToString().Equals("57e697257ae46124486316cb"));
        }

        [Test]
        public void Test_Repository_Recipe_GetAll()
        {
            Repository = new InMemoryTranslationRepository();
            Repository.Fake();
            var items = Repository.GetRecipesInLanguage("zh-CN").AsQueryable<Pizza>();
            Assert.IsTrue(items.ToList().Any());
        }

        [Test]
        public void Test_Repository_Recipe_GetAll_InLanguage()
        {
            Repository = new InMemoryTranslationRepository();
            Repository.Fake();
            var enrecp = Repository.GetRecipesInLanguage(new CultureInfo("zh-CN").Name);
            Assert.IsTrue(enrecp?.First().Contents?.Count>0);  
        }

        [Test]
        public void Test_Repository_Recipe_GetById_InLanguage()
        {
            Repository = new InMemoryTranslationRepository();
            Repository.Fake();
            string testId = "57e697257ae46124486316cb";
            var enrecp = Repository.GetRecipeByIdInLanguage(testId, new CultureInfo("zh-CN").Name);
            Assert.IsTrue(enrecp?.Contents?.Count == 1 && enrecp.Contents.First().LanguageCode.Equals("zh-CN"));  //.Contents.Where(i => !i.LanguageCode.Equals("en-GB")) == null)
        }

    }
}
