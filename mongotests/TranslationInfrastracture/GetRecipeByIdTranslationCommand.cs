﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;

namespace TranslationInfrastracture
{
    public  class GetRecipeByIdTranslationCommand : ITranslationCommand<Pizza>
    {
        public string Type { get; set; }

        public GetRecipeByIdTranslationCommand(CultureInfo culture, string Id)
        {
            CommandCulture = culture;
        }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;
        public Pizza Result { get; }
        public CultureInfo CommandCulture { get; set; }
        public void SetExecutionCulture(CultureInfo info)
        {
            throw new NotImplementedException();
        }
    }
}
