﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TranslationInfrastracture
{

    //public static CultureAwaiter WithCulture(this Task task)
    //{
    //    return new CultureAwaiter(task);
    //}

    public class CultureAwaiter : INotifyCompletion
    {
        private readonly TaskAwaiter m_awaiter;
        private CultureInfo m_culture;

        public CultureAwaiter(Task task)
        {
            if (task == null) throw new ArgumentNullException("task");
            m_awaiter = task.GetAwaiter();
        }

        public CultureAwaiter GetAwaiter() { return this; }

        public bool IsCompleted { get { return m_awaiter.IsCompleted; } }

        public void OnCompleted(Action continuation)
        {
            m_culture = Thread.CurrentThread.CurrentCulture;
            m_awaiter.OnCompleted(continuation);
        }

        public void GetResult()
        {
            Thread.CurrentThread.CurrentCulture = m_culture;
            m_awaiter.GetResult();
        }
    }

}
