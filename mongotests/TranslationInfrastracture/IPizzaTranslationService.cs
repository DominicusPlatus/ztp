﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;

namespace TranslationInfrastracture
{
    public interface IPizzaTranslationService
    {
        Pizza GetRecipeById(string id);
        List<Pizza> GetAllRecipes();
    }
}
