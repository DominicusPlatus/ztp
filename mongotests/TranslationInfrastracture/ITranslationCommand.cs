﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TranslationInfrastracture
{
    public interface ICommandWithResult<T> : ICommand
    {
        T Result { get; }
    }

    public interface  ITranslationCommand<T> : ICommandWithResult<T>
    {
        CultureInfo CommandCulture { get; set; }
        void SetExecutionCulture(CultureInfo info);
    }

}
