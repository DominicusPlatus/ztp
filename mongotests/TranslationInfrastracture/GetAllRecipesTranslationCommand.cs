﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;

namespace TranslationInfrastracture
{
    public class GetAllRecipesTranslationCommand : ITranslationCommand<List<Pizza>>
    {

        public void SetExecutionCulture(CultureInfo info)
        {
            CommandCulture = info;
        }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;
        public List<Pizza> Result { get; }
        public CultureInfo CommandCulture { get; set; }
    }
}
