﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using TranslationInfrastracture;
using TranslationService;

namespace PizzeriaService
{

    public class PizzeriaTranslationApi : IPizzaTranslationService
    {
        private ITranslationServiceRouter TranslationRouter;

        public PizzeriaTranslationApi(ITranslationServiceRouter CoRChain)
        {
            TranslationRouter = CoRChain;
        }

        public Pizza GetRecipeById(string id)
        {
            return TranslationRouter.GetRecipeById(id);
        }
        
        public List<Pizza> GetAllRecipes()
        {
            return TranslationRouter.GetAllRecipes();
        }
    }
}
