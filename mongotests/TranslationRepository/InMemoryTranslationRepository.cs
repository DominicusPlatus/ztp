﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;
using Newtonsoft.Json;

namespace TranslationRepository
{

    public class InMemoryTranslationRepository : ITranslationRepository
    {
        public  List<Pizza> Recipes;

        public bool DoesTranslationExistForRecipeLanguage(string id, string langcode)
        {
            var firstOrDefault = Recipes
                .AsQueryable()
                .FirstOrDefault(i => i.Id.Equals(id));
            return
                firstOrDefault != null && firstOrDefault
                        .Contents.Any(c => c.LanguageCode.Equals(langcode));
        }


        public Pizza GetRecipeByIdInLanguage(string id, string langcode)
        {
            Pizza result = new Pizza();
            var firstOrDefault = Recipes.AsQueryable()
                .First(i => i.Id.Equals(id));
            if (firstOrDefault != null)
            {
                result = firstOrDefault;
                result.Contents = firstOrDefault.Contents.Where(c => c.LanguageCode.Equals(langcode)).ToList() as ICollection<PizzaContent>;
            }
            return result;
        }

        public List<Pizza> GetRecipesInLanguage(string langcode)
        {
            List<Pizza> result = new List<Pizza>();
            foreach (var item in Recipes.AsQueryable())
            {
                var npizza = item;
                npizza.Contents = item.Contents.Where(c => c.LanguageCode.Equals(langcode)).ToList() as ICollection<PizzaContent>;
                if (npizza.Contents.Count > 0)
                {
                    result.Add(npizza);
                }

            }
            return result;
        }


        public InMemoryTranslationRepository()
        {
            Recipes = new List<Pizza>();
        }

        public void Fake()
        {
            Pizza recipe1 = new Pizza();
            recipe1.Contents = new List<PizzaContent>();
            recipe1.Id = "57e697257ae46124486316cb";    //Guid.NewGuid().ToString();
            recipe1.ImageUrl = Guid.NewGuid().ToString();
            recipe1.Price = 11.11;

            recipe1.Contents.Add(new PizzaContent()
            {
                Description = Guid.NewGuid().ToString(),
                LanguageCode = "zh-CN",
                Name = Guid.NewGuid().ToString()
            });
            recipe1.Contents.Add(new PizzaContent()
            {
                Description = Guid.NewGuid().ToString(),
                LanguageCode = "de-DE",
                Name = Guid.NewGuid().ToString()
            });
            recipe1.Contents.Add(new PizzaContent()
            {
                Description = Guid.NewGuid().ToString(),
                LanguageCode = "fr-FR",
                Name = Guid.NewGuid().ToString()
            });

            Recipes.Add(recipe1);

        }

    }

}
