﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using TranslationRepository;

namespace mongotests
{
    public class MongoTranslationRepository : ITranslationRepository
    {
        private IMongoDatabase Database { get; set; }
        private readonly IMongoCollection<Pizza> Recipes;

        public bool DoesTranslationExistForRecipeLanguage(string id, string langcode)
        {
            var firstOrDefault = Recipes
                .AsQueryable()
                .FirstOrDefault(i => i.Id.Equals(id));
            return
                firstOrDefault != null && firstOrDefault
                        .Contents.First(c => c.LanguageCode.Equals(langcode)) != null;
        }


        public Pizza GetRecipeByIdInLanguage(string id, string langcode)
        {
            Pizza result = new Pizza();
            var firstOrDefault = Recipes.AsQueryable()
                .First(i => i.Id.Equals(id));
            if (firstOrDefault != null)
            {
                result = firstOrDefault;
                result.Contents = firstOrDefault.Contents.Where(c => c.LanguageCode.Equals(langcode)).ToList() as ICollection<PizzaContent>;
            }
            return result;
        }

        public List<Pizza> GetRecipesInLanguage(string langcode)
        {
            List<Pizza> result = new List<Pizza>();
            foreach (var item in Recipes.AsQueryable())
            {
                var npizza = item;
                npizza.Contents = item.Contents.Where(c => c.LanguageCode.Equals(langcode)).ToList() as ICollection<PizzaContent>;
                if (npizza.Contents.Count > 0)
                {
                    result.Add(npizza);
                }
             
            }
            return result;
        }

        public void Fake()
        {
            

        }


        public MongoTranslationRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            Database = client.GetDatabase("local");
            Recipes = Database.GetCollection<Pizza>("pizza");
        }

    }
}
