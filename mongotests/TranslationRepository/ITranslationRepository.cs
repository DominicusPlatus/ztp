﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mongotests;

namespace TranslationRepository
{
    public interface ITranslationRepository
    {
        bool DoesTranslationExistForRecipeLanguage(string id, string code);
        Pizza GetRecipeByIdInLanguage(string id, string langcode);
        List<Pizza> GetRecipesInLanguage(string langcode);
        void Fake();
    }


}
